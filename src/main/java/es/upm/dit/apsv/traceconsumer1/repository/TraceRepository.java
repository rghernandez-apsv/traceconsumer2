package es.upm.dit.apsv.traceconsumer1.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import es.upm.dit.apsv.traceconsumer1.model.Trace;
@Repository
public interface TraceRepository extends CrudRepository<Trace,String> {
}
