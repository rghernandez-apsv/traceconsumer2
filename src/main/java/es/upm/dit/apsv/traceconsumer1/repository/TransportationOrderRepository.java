package es.upm.dit.apsv.traceconsumer1.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import es.upm.dit.apsv.traceconsumer1.model.TransportationOrder;

@Repository
public interface TransportationOrderRepository extends CrudRepository<TransportationOrder,String> {
    TransportationOrder findByTruckAndSt(String truck, int st);
}


