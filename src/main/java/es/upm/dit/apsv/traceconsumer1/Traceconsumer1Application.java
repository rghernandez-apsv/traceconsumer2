package es.upm.dit.apsv.traceconsumer1;


import org.hibernate.validator.internal.util.logging.Log;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

import org.springframework.core.env.Environment;

import org.springframework.web.client.HttpClientErrorException;

import org.springframework.web.client.RestTemplate;

import java.util.NoSuchElementException;
import java.util.function.Consumer;

import es.upm.dit.apsv.traceconsumer1.model.Trace;
import es.upm.dit.apsv.traceconsumer1.model.TransportationOrder;
import es.upm.dit.apsv.traceconsumer1.repository.TraceRepository;
import es.upm.dit.apsv.traceconsumer1.repository.TransportationOrderRepository;

@SpringBootApplication
public class Traceconsumer1Application {

	private static final Logger log = LoggerFactory.getLogger(Traceconsumer1Application.class);

	@Autowired
	private TraceRepository traceRepository;

	@Autowired
	private TransportationOrderRepository transportationOrderRepository;


	public static void main(String[] args) {
		SpringApplication.run(Traceconsumer1Application.class, args);
	}


	@Bean("consumer")
	public Consumer<Trace> checkTrace() {
		return t -> {
			t.setTraceId(t.getTruck() + t.getLastSeen());
			traceRepository.save(t);
			
			
			TransportationOrder result = null;
			try {
				result = transportationOrderRepository.findById(t.getTruck()).get();
			} catch (NoSuchElementException ex) {
				result = null;
			}
			if (result != null && result.getSt() == 0) {
				result.setLastDate(t.getLastSeen());
				result.setLastLat(t.getLat());
				result.setLastLong(t.getLng());
				if (result.distanceToDestination() < 10)
					result.setSt(1);
				transportationOrderRepository.save(result);
				log.info("Order updated: " + result);

			}

		};


	}

	

}
